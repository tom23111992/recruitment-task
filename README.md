# Recruitment Task

###Included libraries/technologies
 
 - Java
 - Test-NG
 - Selenide
 - Allure Report
 - Java Faker (generating random data)
 - unitils - (checking objects - ReflectionAssert)
 
###Running
Test can be run on two ways:

- `mvn clean test` - it uses default selenide configuration 
- `mvn clean test -Dbrowser=chrome `- it uses webdriver defined in WebDriverManagement class 