package org.task.objects;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@AllArgsConstructor
public class Account {
    private String email;
    private String password;
    private String rePassword;
    private String jobCorpsCenter;
    private String hearAboutUs;
}
