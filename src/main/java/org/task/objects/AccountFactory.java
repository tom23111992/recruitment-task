package org.task.objects;

import com.github.javafaker.Faker;

public class AccountFactory {

    public static Account getCorrectAccount() {
        Faker faker = new Faker();

        String password = "A" + faker.internet().password(6, 15) + "12!@";
        return new Account(faker.internet().emailAddress(), password, password, "", "");
    }
}
