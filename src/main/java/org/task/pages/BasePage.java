package org.task.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;

public abstract class BasePage {

    public void waitForModule(By element) {
        boolean value = false;
        while (!value) {
            $(".logo").waitUntil(Condition.visible, 10000);
            if ($(".page-errors-content").is(Condition.visible)) {
                refresh();
            } else if ($(element).is(Condition.visible)) value = true;
        }
    }
}
