package org.task.pages;

import com.codeborne.selenide.Condition;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.task.objects.Account;

import static com.codeborne.selenide.Selenide.$;

public class CreateAccountFormPage extends BasePage {

    private By emailField = By.id("c_elem_0");
    private By passwordField = By.id("a_elem_1");
    private By rePasswordField = By.id("a_elem_2");
    private By jobCorpsCenterDropdownList = By.id("elem_3");
    private By hearAboutDropdownList = By.id("elem_4");

    private By termsAccept = By.id("id_option_label_elem_5-true");

    private By passwordRequirementsInfo = By.name("passwordAssistForm");
    private By createAccountButton = By.xpath("//button[contains(text(),'Create an account')]");


    public CreateAccountFormPage() {
        waitForModule(passwordRequirementsInfo);
    }

    /**
     * Method fills crete account form
     *
     * @param account
     * @return account
     */
    public Account fillForm(Account account) {
        Account upAccount = account;
        fillEmail(account.getEmail());
        fillPassword(account.getPassword());
        fillRePassword(account.getRePassword());

        upAccount.setJobCorpsCenter(selectJobsCorpsCenter());
        upAccount.setHearAboutUs(selectHearAboutUs());

        clickAcceptTerms();

        return upAccount;
    }

    /**
     * Method clicks create account button
     *
     * @return DashboardPage
     */
    public DashboardPage clickCreateAccount() {
        $(createAccountButton).should(Condition.enabled).click();
        return new DashboardPage();
    }

    private void fillEmail(String value) {
        $(emailField).sendKeys(value);
    }

    private void fillPassword(String value) {
        $(passwordField).sendKeys(value);
    }

    private void fillRePassword(String value) {
        $(rePasswordField).sendKeys(value);
    }

    private String selectJobsCorpsCenter() {
        int quantity = $(jobCorpsCenterDropdownList).getSelectedOptions().size();

        Faker faker = new Faker();
        $(jobCorpsCenterDropdownList).selectOption(faker.number().numberBetween(0, quantity));
        return $(jobCorpsCenterDropdownList).getSelectedOption().getText();
    }

    private String selectHearAboutUs() {
        int quantity = $(hearAboutDropdownList).getSelectedOptions().size();

        Faker faker = new Faker();
        $(hearAboutDropdownList).selectOption(faker.number().numberBetween(0, quantity));
        return $(hearAboutDropdownList).getSelectedOption().getText();
    }

    private void clickAcceptTerms() {
        $(termsAccept).should(Condition.enabled).click();
    }
}