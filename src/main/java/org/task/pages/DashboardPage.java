package org.task.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class DashboardPage extends BasePage {

    private By dashboardView = By.id("DashboardView");
    private By philipsJobsButton = By.xpath("//*[@class='row footer-links']//a");

    public DashboardPage() {
        waitForModule(dashboardView);
    }

    /**
     * Method clicks philips jobs button
     *
     * @param value
     * @return JobOfferPage
     */
    public JobOfferPage clickPhilipsJobs(String value) {
        $$(philipsJobsButton).find(Condition.text(value)).scrollTo().click();
        return new JobOfferPage();
    }
}