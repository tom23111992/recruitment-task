package org.task.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.task.objects.JobOffer;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class JobOfferPage extends BasePage {

    private By myJobSearch = By.id("dropdown-My-job-search");
    private By savedJobsLink = By.cssSelector(".dropdown-menu li >a");

    private By jobOfferList = By.cssSelector("div[id='SearchResults'] > section");
    private By saveOfferButton = By.id("SaveButton");
    private By alertSuccess = By.cssSelector(".alert-success");
    private By alertCloseButton = By.cssSelector(".alert-success .mux-close");

    private By positionCard = By.cssSelector(".summary .title");
    private By companyNameCard = By.cssSelector(".summary .company");

    private By detailsContent = By.cssSelector(".details-content");

    public JobOfferPage() {
        waitForModule(myJobSearch);
    }

    /**
     * Method clicks open save jobs from menu
     *
     * @return CreateAccountFormPage
     */
    public SavedJobsPage openSaveJobs() {
        $(myJobSearch).hover();
        $$(savedJobsLink).find(Condition.text("Saved Jobs")).click();

        return new SavedJobsPage();
    }

    /**
     * Method clicks save button
     *
     * @return void
     */
    public void saveJobOffer() {
        $(saveOfferButton).should(Condition.enabled).click();
        closeSuccessAlert();
    }

    /**
     * Method clicks last job offer
     *
     * @return JobOffer
     */
    public JobOffer selectLastOffer() {
        int size = $$(jobOfferList).size();
        $$(jobOfferList).last().should(Condition.enabled).click();
        waitForJobDesc();
        return new JobOffer(getCompanyName(size - 1), getPositionName(size - 1));
    }

    /**
     * Method clicks job offer by index
     *
     * @param index
     * @return JobOffer
     */
    public JobOffer selectOfferByIndex(int index) {
        $$(jobOfferList).get(index).should(Condition.enabled).click();
        waitForJobDesc();
        return new JobOffer(getCompanyName(index), getPositionName(index));
    }

    private String getCompanyName(int index) {
        return $$(companyNameCard).get(index).getText();
    }

    private String getPositionName(int index) {
        return $$(positionCard).get(index).getText();
    }

    private void waitForJobDesc() {
        $(detailsContent).waitUntil(Condition.visible, 5000);
    }

    private void closeSuccessAlert() {
        $(alertSuccess).should(Condition.visible);
        $(alertCloseButton).click();
        $(alertSuccess).should(Condition.disappear);
    }
}