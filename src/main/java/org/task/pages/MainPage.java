package org.task.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MainPage extends BasePage {

    private By createAccountButton = By.xpath("//span[contains(text(),'CREATE ACCOUNT')]");

    public MainPage() {
        waitForModule(createAccountButton);
    }

    /**
     * Method clicks create account button
     *
     * @return CreateAccountFormPage
     */
    public CreateAccountFormPage clickCreateAccount() {
        $(createAccountButton).should(Condition.enabled).click();
        return new CreateAccountFormPage();
    }
}