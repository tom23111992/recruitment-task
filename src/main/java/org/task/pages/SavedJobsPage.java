package org.task.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.task.objects.JobOffer;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SavedJobsPage extends BasePage {

    private By table = By.cssSelector(".display-table");
    private By position = By.cssSelector(".list .list-item-heading > a");
    private By companyName = By.cssSelector(".list span[title='Company']");

    private By removeButton = By.xpath("//*[contains(@id,'delete')]");

    public SavedJobsPage() {
        waitForModule(table);
    }

    /**
     * Method gets saved job offers from the page
     *
     * @return List<JobOffer>
     */
    public List<JobOffer> getSavedJobOffers() {
        List<JobOffer> offerList = new ArrayList<>();

        $(removeButton).waitUntil(Condition.visible, 2000);

        for (int i = 0; i < $$(position).size(); i++) {
            offerList.add(new JobOffer(getCompanyName(i), getPositionName(i)));
        }

        return offerList;
    }

    private String getCompanyName(int index) {
        return $$(companyName).get(index).getText();
    }

    private String getPositionName(int index) {
        return $$(position).get(index).getText();
    }
}