package org.task.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configurations extends Properties {

    private static final Logger LOGGER = Logger.getLogger(Configurations.class.getName());
    private static String defaultUrl;
    private static Configurations properties;
    private static String configPath = "src/main/resources/config.properties";

    public static String getDefaultUrl() {
        return defaultUrl;
    }

    public static void setProperties() {

        InputStream is = null;
        properties = new Configurations();

        try {
            is = new FileInputStream(configPath);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

        try {
            properties.load(is);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        init();
    }

    private static void init() {
        defaultUrl = properties.getProperty("url");
    }

}
