package org.task.utils;

import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.logging.Logger;

public class WebDriverManagement {

    private static final Logger LOGGER = Logger.getLogger(WebDriverManagement.class.getName());

    public static void setDriver() {
        String browser = System.getProperty("browser");

        if (!(StringUtils.isBlank(browser))) {
            LOGGER.info("You use override driver");
            WebDriverRunner.setWebDriver(setBrowser(browser));
        }
    }

    private static WebDriver setBrowser(String browser) {
        String system = System.getProperty("os.version");
        WebDriver webDriver = null;

        switch (browser) {
            case "chrome":
                if (system.contains("Linux")) {
                    WebDriverManager.chromiumdriver().setup();
                } else {
                    WebDriverManager.chromedriver().setup();
                }

                DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--start-maximized");

                desiredCapabilities.acceptInsecureCerts();
                desiredCapabilities.merge(chromeOptions);

                webDriver = new ChromeDriver(desiredCapabilities);
                break;
        }

        return webDriver;
    }
}
