package org.task;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.selenide.AllureSelenide;
import org.task.pages.*;
import org.task.utils.Configurations;
import org.task.utils.WebDriverManagement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Selenide.open;

@Listeners({ScreenShooter.class})
public abstract class AbstractTest {

    protected MainPage mainPage;
    protected CreateAccountFormPage createAccountFormPage;
    protected DashboardPage dashboardPage;
    protected JobOfferPage jobOfferPage;
    protected SavedJobsPage savedJobsPage;

    public AbstractTest() {
        Configurations.setProperties();
        Configuration.timeout = 10000;
        Configuration.startMaximized = true;
        WebDriverManagement.setDriver();
    }

    @BeforeClass
    public void setup() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        open(Configurations.getDefaultUrl());
    }

    @AfterClass
    public void exit() {
        WebDriverRunner.closeWebDriver();
    }
}
