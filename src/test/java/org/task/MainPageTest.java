package org.task;

import org.task.objects.Account;
import org.task.objects.AccountFactory;
import org.task.objects.JobOffer;
import org.task.pages.MainPage;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import java.util.ArrayList;
import java.util.List;

public class MainPageTest extends AbstractTest {

    @Test
    public void openPage() {
        mainPage = new MainPage();
        createAccountFormPage = mainPage.clickCreateAccount();

        Account account = createAccountFormPage.fillForm(AccountFactory.getCorrectAccount());

        dashboardPage = createAccountFormPage.clickCreateAccount();
        jobOfferPage = dashboardPage.clickPhilipsJobs("Philips Jobs");

        JobOffer jobOffer = jobOfferPage.selectOfferByIndex(1);
        jobOfferPage.saveJobOffer();


        JobOffer jobOffer1 = jobOfferPage.selectLastOffer();
        jobOfferPage.saveJobOffer();

        List<JobOffer> saveJobOfferList = new ArrayList<>();
        saveJobOfferList.add(jobOffer);
        saveJobOfferList.add(jobOffer1);

        List<JobOffer> savedJobOfferList = jobOfferPage.openSaveJobs().getSavedJobOffers();

        ReflectionAssert.assertLenientEquals("Saved jobs lists are not the same", saveJobOfferList, savedJobOfferList);
    }
}